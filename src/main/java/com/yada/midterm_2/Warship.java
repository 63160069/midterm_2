/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.midterm_2;

/**
 *
 * @author ASUS
 */
public class Warship extends Ship{
    Warship(double L,double B,double D){
        super(L,B,D);
    }
    public double calWeight(){
        return 0.84*(L*B*D)/100;
    }
    
    public void print(String name) {
        System.out.println("WarShip: "+name+" "+calWeight()+" cubic meter");
    }
    
    public void print() {
        System.out.println("Warship: "+calWeight()+" cubic meter");
    }
}
