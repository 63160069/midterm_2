/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.midterm_2;

/**
 *
 * @author ASUS
 */
public class Ship {
    protected double L;
    protected double B;
    protected double D;
    public Ship(double L,double B,double D) {
        this.L = L;
        this.B = B;
        this.D = D;
    }
    
    public double calWeight(){
        return 0.5*(L*B*D)/100;
    }
    
    public void print(String name) {
        System.out.println("Ship: "+name+" "+calWeight()+" cubic meter");
    }
    public void print() {
        System.out.println("Ship: "+calWeight()+" cubic meter");
    }
    
}
